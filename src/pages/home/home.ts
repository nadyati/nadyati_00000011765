import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirstPage } from '../first-page/first-page';
import { SecondPage } from '../second-page/second-page';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
  }
  Openpage1(){
  	this.navCtrl.push(FirstPage);  	
  	}

 Openpage2(){
  	this.navCtrl.push(SecondPage);  	
  	}

}
